var Intro = View.extend({
    
    id: 'intro',
    
    template: 'intro.html',
    
    css: 'intro.css',
    
    className: 'languages',
    
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
    navigate: function(key)    {
		var $curr = $('#intro a.active');
		var $next = $curr.next();
		if($next.length<=0) 
			$next = $curr.prev();

        var ignoredKeys = ['MENU', 'HOME', 'EXIT', 'BACK', 'POWR', 'CLOSE', 'CLOSEALL'];
        if(ignoredKeys.indexOf(key) >= 0) {
            return true;
        }

		else if (key == 'ENTER') {  // default link click             								
			return this.click($curr);
		}
		else if(key == 'UP' || key == 'DOWN') {
            if($next.length > 0) {
                this.blur($curr);
                this.focus($next);
            }
		}
        
        return false;
    },
    
    click: function($jqobj) {
        var linkid = $jqobj.attr('id');
        window.settings = window.settings || {};

		if(linkid == 'mycare') {	
		    // tracking
            nova.tracker.event('intro', 'introset', linkid);

            // go to second intro
            var intro2 = new Intro2({});
            intro2.render();
            return true;			        
		}
		if(linkid == 'watchtv') {                    
            nova.tracker.event('intro', 'watchtv');
            
            // Reset language
            setLanguage('');

            // Close primary page
            window.pages.closePage('primary');

            // Get TV list and launch a videoplayer2 page
            this.watchTV(linkid);
            return true;
        }
                
        return false;
    },
    
    renderData: function() {
        var context = this;
    },
    
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    watchTV: function (className) {
        //var context = this;
        //
        //this.tvChannels = [];
        // var channels = epgChannels().Channels;
        // $.each(channels, function(i, ch) {
        //     context.tvChannels.push({type: 'Analog', url: ch.channelNumber, display: ch.channelName});
        // });
        // var page = new VideoPlayer2({viewId: 'tvonly', className:className, playlist: this.tvChannels, playlistIndex: 0, delay: 10000, delayMessage: 'One moment while we are loading the television channels...'});
        // page.render();

        var linkId = 'tv';
        var pagePath = this.pagePath + '/' + linkId;
        var page = new TVGuide({className: className, pagePath: pagePath});
        page.render();
    }
    
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
});

